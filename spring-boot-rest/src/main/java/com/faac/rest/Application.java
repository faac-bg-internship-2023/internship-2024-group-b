/*
 * @author : neiko.neikov
 * @created : 8.7.2024 г., понеделник
 */
package com.faac.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
