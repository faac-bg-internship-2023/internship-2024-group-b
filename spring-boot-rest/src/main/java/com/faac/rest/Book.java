/*
 * @author : neiko.neikov
 * @created : 8.7.2024 г., понеделник
 */
package com.faac.rest;

public class Book {
    public String title;
    public String author;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }
}
