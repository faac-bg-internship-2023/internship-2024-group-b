/*
 * @author : neiko.neikov
 * @created : 8.7.2024 г., понеделник
 */
package com.faac.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class ContentController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("/content")
    public String hello() {
        return "<h1>Hello World</h1>";
    }

    @GetMapping("/books/all")
    public List<Book> getBooks() {
        return List.of(
                new Book("Don QUihotes", "Servantes"),
                new Book("War & Peace", "Lev Tolstoy"));
    }

    @GetMapping("/books/{id}")
    public Book getBook(@PathVariable String id) {
        return new Book("Don QUihotes", "Servantes");
    }

    @GetMapping("/forecast")
    public String client() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "Weather App");
        String result = restTemplate.exchange(
            "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=43.85246629231725&lon=25.954713417169426", HttpMethod.GET,
                new HttpEntity<>(null, headers), String.class).getBody();
        logger.info(result);
        return result;
    }
}
